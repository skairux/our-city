//
//  SearchTableViewController.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 15/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *Images;
@property (nonatomic, strong) NSArray *Titles;
@property (nonatomic, strong) NSArray *Descriptions;

@end
