//
//  searchResultsTableViewController.m
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 15/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import "searchResultsTableViewController.h"
#import "searchResultsTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import "mapsViewController.h"
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "Venue.h"

@interface searchResultsTableViewController ()

@property (nonatomic, strong) NSArray *venues;

@end

@implementation searchResultsTableViewController


@synthesize locationManager;

- (void)configureRestKit
{

    // Initialization
    NSURL *baseUrl = [NSURL URLWithString:@"http://opendata.paris.fr"];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
    
    // Initialization restkit
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    
    // setup object mappings
    RKObjectMapping *venueMapping = [RKObjectMapping mappingForClass:[Venue class]];
    [venueMapping addAttributeMappingsFromArray:@[@"nom_du_cafe",
                                                  @"nom_etablissement",
                                                  @"adresse",
                                                  @"coordonnees",
                                                  @"geo_latitude",]];
    
    // Register map with the provider
    RKResponseDescriptor *responsedescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:venueMapping
                                            method:RKRequestMethodGET
                                            pathPattern:@"/api/records/1.0/search"
                                            keyPath:@"records.fields"
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    //done
    [objectManager addResponseDescriptor:responsedescriptor];
}

- (void)loadVenues
{
    // setup data for API request
    NSString *dataset;
    NSString *Longitude;
    NSString *Lattitude;
    //NSInteger * row;
    
    // current location
    float longitude = locationManager.location.coordinate.longitude;
    float lattitude = locationManager.location.coordinate.latitude;
    
    // confition for request choice
    if ([_Title[0]  isEqual:@"Cinema"])
        dataset = @"cinemas-a-paris";
    else if ([_Title[0] isEqual:@"Coffee"])
        dataset = @"liste-des-cafes-a-un-euro";
    
    //condition for location
    if (longitude !=0  && lattitude != 0) {
        Longitude = [[NSString alloc] initWithFormat:@"%f", longitude];
        Lattitude = [[NSString alloc] initWithFormat:@"%f", lattitude];
    
        NSDictionary *queryParams = @{@"dataset":        dataset,
                                  @"rows":               @"35",
                                  @"geofilter.distance": [NSString stringWithFormat: @"%@,%@,2000", Lattitude, Longitude]};
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"/api/records/1.0/search"
                                           parameters: queryParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  _venues = mappingResult.array;
                                                  [self.tableView reloadData];
                                              }
                                              failure:^(RKObjectRequestOperation *ooperation, NSError *error) {
                                                  NSLog(@"error: %@", error);
                                              }];
    
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // get location of the phone
    self.locationManager = [[CLLocationManager alloc]
                            init];
    geocoder             = [[CLGeocoder alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    
    
    // IOS 8 Fix with authorization
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [self.locationManager requestAlwaysAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    _Title = @[_ModalLabel[0]];
    
    [self configureRestKit];
    [self loadVenues];
    
    self.navigationItem.title = _ModalLabel[0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _venues.count;
}

// test for location
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil && [placemarks count] > 0) {
            // Stop Location Manager
            [self.locationManager stopUpdatingLocation];
            
        } else {
            NSLog(@"%@", error.debugDescription);
            [self.locationManager stopUpdatingLocation];
        }
    }];
}


// Error when fail
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellResults";
    searchResultsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Venue *venue = _venues[indexPath.row];
    //NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
    
    cell.city.text = venue.nom_etablissement;
    cell.latitude.text = venue.nom_du_cafe;
    cell.adresse.text = venue.adresse;
//    cell.geo_latitude.text = venue.geo_latitude;
//    cell.coordonnees.text = venue.coordonnees;
    return cell;
}

#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ShowInformations"] ) {
        mapsViewController *detailview =  [segue destinationViewController];
        NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
        
        Venue *venue = _venues[myIndexPath.row];
        
        if ([_Title[0] isEqual:@"Cinema"]) {
            detailview.ModalLabel = @[venue.nom_etablissement, venue.adresse, venue.coordonnees[0], venue.coordonnees[1]];
        }
        else if ([_Title[0] isEqual:@"Coffee"]) {
            detailview.ModalLabel = @[venue.nom_du_cafe, venue.adresse, venue.geo_latitude[0], venue.geo_latitude[1]];
        }
    }
}

@end
