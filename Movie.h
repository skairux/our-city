//
//  Movie.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 13/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *backgroundImage;
@property (nonatomic, copy) NSString *littleDescription;

@end