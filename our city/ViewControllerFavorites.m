//
//  ViewControllerFavorites.m
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 10/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import "ViewControllerFavorites.h"
#import "Movie.h"
#import "FavoriteCell.h"

@interface ViewControllerFavorites ()

@property (nonatomic,strong) NSMutableArray *movies;

@end

@implementation ViewControllerFavorites

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.movies = [NSMutableArray arrayWithCapacity:5];
    Movie *movie = [[Movie alloc] init];
    
    
    // prototype text
    movie.title = @"paris";
    movie.littleDescription = @"Paris, une belle ville !";
    movie.backgroundImage = @"Paris.jpg";
    [self.movies addObject:movie];
    
    // add button shader
    [self.ButtonSearch.layer setShadowOffset:CGSizeMake(0.5, 0.5)];
    [self.ButtonSearch.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.ButtonSearch.layer setShadowOpacity:0.7];
    // Do any additional setup after loading the view.
}


// template favorite
//- ( UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *cellIdentifier = @"FavoriteCell";
//    FavoriteCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
//    Movie *movie = (self.movies) [indexPath.row];
//    cell.titleLabel.text = movie.title;
//    cell.descriptionLabel.text = movie.littleDescription;
//    return cell;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.movies count];
}

@end

