//
//  MapPinObject.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 16/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface MapPinObject : NSObject <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;


@end
