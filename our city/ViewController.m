//
//  ViewController.m
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 09/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.button.layer setShadowOffset:CGSizeMake(0.5, 0.5)];
    [self.button.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.button.layer setShadowOpacity:0.2];
     

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
