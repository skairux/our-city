//
//  Venue.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 15/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Venue : NSObject

@property (nonatomic, strong) NSString *nom_etablissement;
@property (nonatomic, strong) NSString *nom_du_cafe;
@property (nonatomic, strong) NSString *adresse;
@property (nonatomic, strong) NSArray *geo_latitude;
@property (nonatomic, strong) NSArray *coordonnees;

@end