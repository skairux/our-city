//
//  SQLTrip.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 14/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SQLTrip : NSObject

@property (nonatomic, strong) NSString *sqlPath;
@property (nonatomic) sqlite3 *dataBase;

@end
