//
//  mapsViewController.m
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 16/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import "mapsViewController.h"
#import "MapPinObject.h"
#import <Social/Social.h>

@interface mapsViewController ()

@end

@implementation mapsViewController
@synthesize mapsView;


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    _latitude  = _ModalLabel[2];
    _longitude = _ModalLabel[3];
    
    // custom location
    CLLocationCoordinate2D coord = {.latitude =  [_latitude doubleValue], .longitude =  [_longitude doubleValue]};
    //MKCoordinateSpan span = {.latitudeDelta =  1, .longitudeDelta =  1};
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 800, 800);
    [self.mapsView setRegion:[self.mapsView regionThatFits:region]];
    
    // place the pind and the desired description
    MapPinObject *pin = [[MapPinObject alloc] init];
    pin.title         = _ModalLabel[0];
    pin.subtitle      = _ModalLabel[1];
    pin.coordinate    = region.center;
    [mapsView addAnnotation:pin];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapsView.delegate = self;
    
    _TitleLabel.text  = _ModalLabel[0];
    _AdressLabel.text = _ModalLabel[1];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)Direction:(id)sender {
    // apple maps /location to destination/
    NSString *baseUrl = @"http://maps.apple.com/maps?daddr";
    NSString *parameters = [NSString stringWithFormat:@"%@=%@,%@",baseUrl, _latitude, _longitude];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:parameters]];
}

- (IBAction)FacebookShare:(id)sender {
        NSString *TextShare                    = [NSString stringWithFormat:@"I want to share you This place: %@. \nNice place and i like it :) \nshare it : %@", _ModalLabel[0], _ModalLabel[1]];
        SLComposeViewController *FacebookShare = [SLComposeViewController
                                                  composeViewControllerForServiceType:SLServiceTypeFacebook];
        [FacebookShare setInitialText:TextShare];
        [self presentViewController:FacebookShare animated:YES completion:nil];
}

@end
