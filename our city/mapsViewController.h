//
//  mapsViewController.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 16/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface mapsViewController : UIViewController <MKMapViewDelegate>

@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) IBOutlet MKMapView *mapsView;
@property (nonatomic, strong) IBOutlet UILabel  *TitleLabel;
@property (nonatomic, strong) IBOutlet UILabel  *AdressLabel;
@property (nonatomic, strong) IBOutlet UILabel  *coordonneesLat;
@property (nonatomic, strong) IBOutlet UILabel  *coordonneesLon;
@property (nonatomic, strong) NSArray *ModalLabel;

- (IBAction)FacebookShare:(id)sender;

// route using maps
-(IBAction)Direction:(id)sender;

@end
