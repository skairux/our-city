//
//  DetailsViewController.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 14/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import "ViewController.h"

@interface DetailsViewController : ViewController

@property (nonatomic, strong) IBOutlet UILabel *TitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *DescriptionLabel;
@property (nonatomic, strong) IBOutlet UIImageView *PosterImageView;

@property (nonatomic, strong) NSArray *ModalLabel;


@end
