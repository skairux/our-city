//
//  searchResultsTableViewCell.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 15/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface searchResultsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *imageTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *latitude;
@property (nonatomic, weak) IBOutlet UILabel *longitude;
@property (nonatomic, weak) IBOutlet UILabel *city;
@property (nonatomic, weak) IBOutlet UIImageView *ImagesView;
@property (nonatomic, weak) IBOutlet UILabel *adresse;

@end
