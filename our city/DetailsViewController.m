//
//  DetailsViewController.m
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 14/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _TitleLabel.text       = _ModalLabel[0];
    _DescriptionLabel.text = _ModalLabel[1];
    _PosterImageView.image = [UIImage imageNamed:_ModalLabel[2]];
    
    self.navigationItem.title = _ModalLabel[0];
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
