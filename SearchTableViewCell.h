//
//  SearchTableViewCell.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 15/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel     *TitleLabel;
@property (nonatomic, weak) IBOutlet UILabel     *DescriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *ImageLabel;

@end
