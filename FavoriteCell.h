//
//  FavoriteCell.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 13/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *TitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *DescriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *PosterImageView;



@end
