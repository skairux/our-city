//
//  searchResultsTableViewController.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 15/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface searchResultsTableViewController : UITableViewController <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    CLGeocoder        *geocoder;
    CLPlacemark       *placemark;
}

@property(nonatomic, strong) NSArray  *Title;
@property(nonatomic, strong) NSArray *FinalTitle;
@property(nonatomic, strong) NSArray *Adresse;
@property(nonatomic) float  longitude;
@property(nonatomic) float  latitude;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSString *City;


@property (nonatomic, strong) NSArray *ModalLabel;

@end
