//
//  tableViewFavoriteController.h
//  our city
//
//  Created by Mahefa ANDRIANIFAHANANA on 13/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tableViewFavoriteController : UITableViewController

@property (nonatomic, strong) NSArray *Images;
@property (nonatomic, strong) NSArray *Title;
@property (nonatomic, strong) NSArray *Description;

@end
